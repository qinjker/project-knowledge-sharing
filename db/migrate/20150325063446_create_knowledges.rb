class CreateKnowledges < ActiveRecord::Migration
  def change
    create_table :knowledges do |t|
      t.string :pro_name
      t.text :skill_of_harvest
      t.text :lift_of_harvest
      t.text :study_plan
      t.text :hope_plan
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
