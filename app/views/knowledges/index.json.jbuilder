json.array!(@knowledges) do |knowledge|
  json.extract! knowledge, :id, :pro_name, :skill_of_harvest, :lift_of_harvest, :study_plan, :hope_plan, :user_id
  json.url knowledge_url(knowledge, format: :json)
end
